const path = require('path')
const webpack = require('webpack')

const mode = 'production'

const webConfig = {
    mode,
    entry: './src/generateTransactionPdfWeb.ts',
    devtool: 'inline-source-map',
    target: 'web',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    output: {
        filename: 'index.web.js',
        path: path.resolve(__dirname, 'dist'),
        libraryTarget: 'commonjs',
    },
    plugins: [
        new webpack.IgnorePlugin(/generateTransactionPdfNode/)
    ]
}

const nodeConfig = {
    mode,
    entry: './src/generateTransactionPdfNode.ts',
    target: 'node',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    output: {
        filename: 'index.node.js',
        path: path.resolve(__dirname, 'dist'),
    },
}

module.exports = [
    webConfig,
    nodeConfig,
]
