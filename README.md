# transaction-pdf-generator

Generates invoice and receipt PDFs for transaction records.

## Installation

```
npm install transaction-pdf-generator
```

or

```
yarn add transaction-pdf-generator
```

## Usage

Generate an empty invoice:

```
import { generateTransactionPdf } from 'transaction-pdf-generator'
import { writeFileSync } from 'fs'

const writeEmptyInvoicePdf = async () => {
    const pdfBuffer = await generateTransactionPdf()
    writeFileSync('empty-invoice.pdf', pdfBuffer)
}

writeEmptyInvoicePdf().catch(err => console.error(err))

// An empty-invoice.pdf file should be found in the current working directory.
```

Generate an invoice using all the available options:

```
import { generateTransactionPdf, TransactionPdfType} from 'transaction-pdf-generator'
import { writeFileSync } from 'fs'

const writeReceiptPdfUsingAllOptions = async () => {
    const pdfBuffer = await generateTransactionPdf({

        // See how a default can be included here:
        // https://pdfmake.github.io/docs/getting-started/server-side/
        fontDescriptors: {
            Times: {
                normal: 'Times-Roman',
                bold: 'Times-Bold',
                italics: 'Times-Italic',
                bolditalics: 'Times-BoldItalic',
            }
        },

        // This uses the image and fit properties as described here:
        // https://pdfmake.github.io/docs/document-definition-object/images/
        logo: {
            image: 'test/test-logo.png',
            fit: [118, 67],
        },
        companyDetails: `Example Company Name
Examplestreet 111 - 12345 Town
billing@example.com`,
        customerAddress: `Dr Robin Exampleperson
Exampleperson's Business Name
Example Street 123 Unit 1A
Exampletown Examplestate 54321
Mycountry`,
        customerTaxId: 'BIZ123456789',

        // E.g. VAT number
        customerTaxIdType: 'Customer Sales Tax Id: ',

        // How the document is identified in your systems
        transactionId: '20010101001',
        transactionDate: (new Date).toLocaleDateString(),
        transactionPdfType: TransactionPdfType.Receipt,

        // paidDate is only shown if the transaction type is Receipt.
        paidDate: (new Date).toLocaleDateString(),
        taxRate: 0.1,

        // E.g. VAT, GST, or Sales Tax
        taxType: 'Generic Sales Tax',
        items: [
            {
                quantity: 2,
                description: 'Example service/product description 1',
                date: (new Date).toLocaleDateString(),
                unitPrice: 100,
            },
            {
                quantity: 1,
                description: 'Example service/product description 2',
                date: (new Date).toLocaleDateString(),
                unitPrice: 500,
            },
            {
                quantity: 1,
                description: 'Example service/product description 3',
                date: (new Date).toLocaleDateString(),
                unitPrice: 400,
            }
        ],
        currency: 'EUR',
        decimalPlaces: 2,
        paymentMethods: [
            {
                name: 'Bank transfer',
                details: `IBAN: XX11 1111 1111 1111 1111 11
Bank: Example Bank
Description: Example Description
Recipient: Example Company Name`,
            }
        ],
        supportText: [
            'Send us an email: ',
            {
                text: 'support@example.com',
                link: 'mailto:support@example.com?subject=billing',
            },
            '.'
        ],
        thankYouText: 'Thank you for your business!',
        footerText: `Commercial registery: XXX 111111, Location
Directors: Person One, Person Two
Sales Tax Id: XX 111 111 111`
    })
    writeFileSync('all-options-receipt.pdf', pdfBuffer)
}

writeReceiptPdfUsingAllOptions().catch(err => console.error(err))
```
