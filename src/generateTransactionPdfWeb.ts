const pdfMake = require('pdfmake/build/pdfmake.min')
const pdfFonts = require('pdfmake/build/vfs_fonts')

import { GenerateTransactionPdfParams } from 'transaction-pdf-generator-doc-def'
import { generateDocumentDef } from 'transaction-pdf-generator-doc-def'

pdfMake.vfs = pdfFonts.pdfMake.vfs

export const generateTransactionPdf = async (
    params: GenerateTransactionPdfParams = {},
): Promise<any> => {
    const fontDescriptors = params.fontDescriptors || {
        Helvetica: {
            normal: 'Helvetica',
            bold: 'Helvetica-Bold',
            italics: 'Helvetica-Oblique',
            bolditalics: 'Helvetica-BoldOblique'
        }
    }

    const docDefinition = generateDocumentDef(params, fontDescriptors)
    return new Promise((resolve, reject) => {
        try {
            pdfMake.createPdf(docDefinition)
            .getBuffer((buffer: any) => resolve(buffer))
        }
        catch (err) {
            reject(err)
        }
    })
}
