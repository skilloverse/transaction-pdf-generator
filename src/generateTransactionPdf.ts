import PdfPrinter from 'pdfmake'

import { GenerateTransactionPdfParams } from 'transaction-pdf-generator-doc-def'
import { generateDocumentDef } from 'transaction-pdf-generator-doc-def'

export const generateTransactionPdf = async (
    params: GenerateTransactionPdfParams = {},
): Promise<Buffer> => {
    const fontDescriptors = params.fontDescriptors || {
        Helvetica: {
            normal: 'Helvetica',
            bold: 'Helvetica-Bold',
            italics: 'Helvetica-Oblique',
            bolditalics: 'Helvetica-BoldOblique'
        }
    }

    const docDefinition = generateDocumentDef(params, fontDescriptors)

    return new Promise((resolve, reject) => {
        const printer = new PdfPrinter(fontDescriptors)
        const doc = printer.createPdfKitDocument(docDefinition, {})
        const chunks = [];
        doc.on('error', (err: Error) => {
            reject(err)
        })
        doc.on('data', (chunk: any) => {
            chunks.push(chunk)
        })
        doc.on('end', () => {
            resolve(Buffer.concat(chunks))
        })
        doc.end()
    })
}
